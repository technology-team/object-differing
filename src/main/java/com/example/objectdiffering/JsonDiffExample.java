package com.example.objectdiffering;

import com.example.objectdiffering.dto.Member;
import com.example.objectdiffering.dto.Role;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.flipkart.zjsonpatch.JsonDiff;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class JsonDiffExample {

    public static void main(String[] args) {
        Member original = Member.builder()
                                .id(1)
                                .email("test@email.com")
                                .password("password")
                                .role(Role.builder()
                                          .id(1)
                                          .role("ROLE_ADMIN")
                                          .build())
                                .role(Role.builder()
                                          .id(3)
                                          .role("ROLE_USER")
                                          .build())
                                .parent(Member.builder()
                                              .id(2)
                                              .email("test@email.com")
                                              .password("password")
                                              .build())
                                .build();

        Member change = Member.builder()
                              .id(1)
                              .email("changeTest@email.com")
                              .role(Role.builder()
                                        .id(1)
                                        .role("ROLE_ADMIN")
                                        .build())
                              .parent(Member.builder()
                                            .id(3)
                                            .email("test@email.com")
                                            .password("password")
                                            .build())
                              .build();

        ObjectMapper mapper = new ObjectMapper();
        JsonNode diffNode = JsonDiff.asJson(mapper.valueToTree(original), mapper.valueToTree(change));
        System.out.println(diffNode.toPrettyString());
    }
}
