package com.example.objectdiffering;

import java.util.Map;

import com.example.objectdiffering.dto.Member;
import com.example.objectdiffering.dto.Role;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.MapDifference;
import com.google.common.collect.MapDifference.ValueDifference;
import com.google.common.collect.Maps;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MapDiffExample {

    public static void main(String[] args) {
        Member original = Member.builder()
                                .id(1)
                                .email("test@email.com")
                                .password("password")
                                .role(Role.builder()
                                          .id(1)
                                          .role("ROLE_ADMIN")
                                          .build())
                                .role(Role.builder()
                                          .id(3)
                                          .role("ROLE_USER")
                                          .build())
                                .parent(Member.builder()
                                              .id(2)
                                              .email("test@email.com")
                                              .password("password")
                                              .build())
                                .build();

        Member change = Member.builder()
                              .id(1)
                              .email("changeTest@email.com")
                              .phone("010-1234-5678")
                              .role(Role.builder()
                                        .id(1)
                                        .role("ROLE_ADMIN")
                                        .build())
                              .parent(Member.builder()
                                            .id(3)
                                            .email("test@email.com")
                                            .password("password")
                                            .build())
                              .build();

        ObjectMapper mapper = new ObjectMapper();
        Map originalMap = mapper.convertValue(original, Map.class);
        Map changeMap = mapper.convertValue(change, Map.class);
        MapDifference<String, Object> difference = Maps.difference(originalMap, changeMap);
        Map<String, ValueDifference<Object>> differenceMap = difference.entriesDiffering();

//        for(Map.Entry<String, ValueDifference<Object>> entry : differenceMap.entrySet()) {
//            String leftValue = (String) entry.getValue().leftValue();
//            String rightValue = (String) entry.getValue().rightValue();
//        }

        difference.entriesDiffering().forEach((key, value) -> System.out.println(key + ": " + value));
        //difference.entriesOnlyOnLeft().forEach((key, value) -> System.out.println(key + ": " + value));
        //difference.entriesOnlyOnRight().forEach((key, value) -> System.out.println(key + ": " + value));
        //difference.entriesInCommon().forEach((key, value) -> System.out.println(key + ": " + value));
    }
}
