package com.example.objectdiffering;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ObjectDifferingApplication {

    public static void main(String[] args) {
        SpringApplication.run(ObjectDifferingApplication.class, args);
    }

}
