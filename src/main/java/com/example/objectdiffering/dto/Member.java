package com.example.objectdiffering.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Singular;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Member {
    private int id;

    private String email;

    private String password;

    private String address;

    private String phone;

    @Singular
    private List<Role> roles;

    private Member parent;
}
