package com.example.objectdiffering.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Getter;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Role {
    private int id;

    private String role;
}
